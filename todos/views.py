from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList, TodoItem
from .forms import TodoListForm, TodoItemForm


def todo_list_view(request):
    todos = TodoList.objects.all()
    context = {"todos": todos}
    return render(request, "todos/todo_list.html", context)


def todo_list_detail(request, id):
    todo = get_object_or_404(TodoList, id=id)
    tasks = TodoItem.objects.filter(list=todo)
    context = {
        "todo": todo,
        "tasks": tasks,
    }
    return render(request, "todos/todo_list_detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            TodoList = form.save(commit=False)
            TodoList.save()
            todo_list_id = TodoList.id
            return redirect("todo_list_detail", id=todo_list_id)
    else:
        form = TodoListForm()
    context = {"form": form}
    return render(request, "todos/todo_list_create.html", context)


def todo_list_edit(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    form = TodoListForm(instance=todolist)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            form.save()
            todo_list_id = todolist.id
            return redirect("todo_list_detail", id=todo_list_id)
    context = {"form": form, "todolist": todolist}
    return render(request, "todos/todo_list_edit.html", context)


def todo_list_delete(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todolist.delete()
        return redirect("todo_list_list")
    context = {"todolist": todolist}
    return render(request, "todos/todo_list_delete.html", context)


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        id = request.POST["list"]
        todolist = get_object_or_404(TodoList, id=id)
        if form.is_valid():
            item = form.save(commit=False)
            item.list = todolist
            item.save()
            todo_list_id = todolist.id
            return redirect("todo_list_detail", id=todo_list_id)
    else:
        form = TodoItemForm()
    context = {"form": form}
    return render(request, "todos/todo_item_create.html", context)


def todo_item_update(request, list_id, item_id):
    todolist = get_object_or_404(TodoList, id=list_id)
    item = get_object_or_404(TodoItem, id=item_id, list=todolist)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=list_id)
    else:
        form = TodoItemForm(instance=item)
    return render(
        request,
        "todos/todo_item_update.html",
        {"todolist": todolist, "form": form},
    )
